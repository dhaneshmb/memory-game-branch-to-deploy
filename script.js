const gameContainer = document.getElementById("game");
const button = document.getElementsByTagName("button");

const COLORS = [
  "red",
  "blue",
  "green",
  "orange",
  "purple",
  "red",
  "blue",
  "green",
  "orange",
  "purple"
];

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

let shuffledColors = shuffle(COLORS);

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(colorArray) {
  for (let color of colorArray) {
    // create a new div
    const newDiv = document.createElement("div");

    // give it a class attribute for the value we are looping over
    newDiv.classList.add(color);

    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick);
    // append the div to the element with an id of game
    gameContainer.append(newDiv);
  }
}

// TODO: Implement this function!

let clickCount = 0;
let matchCount = 0;
let allow = true;
let resetElements = [];
let element;

// for checking whether the two boxes are having the same backgroundColor 
function check(element1, element2) {
  if (element1.classList.value !== element2.classList.value) {
    element1.removeAttribute("id");
    element2.removeAttribute("id");
  } else {
    resetElements.push(element1, element2)
    matchCount += 1;
  }
  if (matchCount === 5) {
    alert("you won the match!!!")
  }
  clickCount = 0;
  allow = true;
}

function handleCardClick(event) {
  clickCount += 1;
  let elementColor = event.target.classList.value;
  if (event.target.id !== `${elementColor}color`) {
    if (clickCount < 2 && allow) {
      event.target.id = `${elementColor}color`;
      element1 = event.target;

    } else if (allow) {
      event.target.id = `${elementColor}color`;
      element2 = event.target;
      allow = false;
      setTimeout(check, 500, element1, element2);
    }
  }
}

//for reset

function clear() {
  resetElements.forEach(ele => {
    ele.id = "transparent";
  })
  clickCount = 0;
  matchCount = 0;
  allow = true;
}
button[0].addEventListener("click", clear);

// when the DOM loads
createDivsForColors(shuffledColors);
